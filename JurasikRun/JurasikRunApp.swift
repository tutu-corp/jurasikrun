//
//  JurasikRunApp.swift of project named JurasikRun
//
//  Created by Aurore D (@Tuwleep) on 04/05/2023.
//
//  

import SwiftUI

@main
struct JurasikRunApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
