//
//  ContentView.swift of project named JurasikRun
//
//  Created by Aurore D (@Tuwleep) on 04/05/2023.
//
//  

import SwiftUI

struct ContentView: View {
    
    var runCount = 8
    var deadCount = 8
    var jumpCount = 12
    
    @State var action = "Jump"
    @State var currentSprite = 1
    
    func getImage() -> String {
        let base = action
        let end = "(\(currentSprite))"
        return base + " " + end
    }
    
    func updateSprite() {
        if action == "Jump" {
            if currentSprite == jumpCount {
                currentSprite = 1
            } else {
                currentSprite += 1
            }
        } else {
            if currentSprite == runCount {
                currentSprite = 1
            } else {
                currentSprite += 1
            }
        }
    }
    
    var body: some View {
        VStack {
            Text("Jurasik Run")
                .font(.custom("Jurassic Park", size: 75))
                .foregroundColor(.primary)
                .shadow(color: .red, radius: 0, x: 1, y: 1)
            
            Image(getImage())
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(height: 250)
                .background(.gray)
                .cornerRadius(25)
            
            Spacer()
            Button {
                updateSprite()
            } label: {
                Image(systemName: "play.circle.fill")
                    .font(.system(size: 50))
            }
            HStack {
                ActionButton(buttonTitle: "Cours", action: $action, currentSprite: $currentSprite)
                Spacer()
                ActionButton(buttonTitle: "Saute", action: $action, currentSprite: $currentSprite)
                Spacer()
                ActionButton(buttonTitle: "GameOver", action: $action, currentSprite: $currentSprite)
            }
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
