//
//  ActionButton.swift of project named JurasikRun
//
//  Created by Aurore D (@Tuwleep) on 04/05/2023.
//
//  

import SwiftUI

struct ActionButton: View {
    
    var buttonTitle: String
    @Binding var action: String
    @Binding var currentSprite: Int
    
    var body: some View {
        Button(buttonTitle) {
            currentSprite = 1
            switch buttonTitle {
            case "Cours": action = "Run"
            case "Saute": action = "Jump"
            case "GameOver": action = "Dead"
            default: break
            }
            
        }.buttonStyle(.borderedProminent)
    }
}

struct ActionButton_Previews: PreviewProvider {
    static var previews: some View {
        ActionButton(buttonTitle: "Cours", action: .constant("Run"), currentSprite: .constant(1))
    }
}
